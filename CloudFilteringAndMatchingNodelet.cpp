#include <string>

#include <ros/ros.h>
#include <ros/time.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/registration/registration.h>
#include <pcl/registration/ndt.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>

#include <pclomp/ndt_omp.h>
#include <pclomp/gicp_omp.h>

#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>

#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>

#include <GraphSlam/Keyframe.hpp>
#include <GraphSlam/Utils.hpp>

namespace graph_slam {
    class CloudFilteringAndMatchingNodelet : public nodelet::Nodelet {
    public:
        typedef pcl::PointXYZI PointT;
        typedef pcl::PointCloud<PointT> CloudT;

        CloudFilteringAndMatchingNodelet() {}
        virtual ~CloudFilteringAndMatchingNodelet() {}

        virtual void onInit() {
            m_nodeHandle = getNodeHandle();
            m_privNodeHandle = getPrivateNodeHandle();

            m_sBaseFrame = m_privNodeHandle.param<std::string>("base_frame", "base");
            m_sOdomFrame = m_privNodeHandle.param<std::string>("odom_frame", "odom");

            m_bTransformationThresholding = m_privNodeHandle.param<bool>("odom_thresholding", false);
            m_dMaxTranslationDelta = m_privNodeHandle.param<double>("odom_max_trans", 1.0);
            m_dMaxAngularDelta = m_privNodeHandle.param<double>("odom_max_angle", 1.0);

            m_dMinKeyframeAngularDelta = m_privNodeHandle.param<double>("keyframe_delta_angle", 1.0);
            m_dMinKeyframeTranslationDelta = m_privNodeHandle.param<double>("keyframe_delta_trans", 1.0);
            m_dMinKeyframeTimeDelta = m_privNodeHandle.param<double>("keyframe_delta_time", 1.0);

            m_sPointsTopic = m_privNodeHandle.param<std::string>("points_topic", "/velodyne_points");
            std::cout << "[Point topic] " << m_sPointsTopic << std::endl;
            m_lidarSubscriber = m_nodeHandle.subscribe(m_sPointsTopic, 64, &CloudFilteringAndMatchingNodelet::cloudCallback, this);
            m_filteredCloudPublisher = m_nodeHandle.advertise<sensor_msgs::PointCloud2>("/filtered_points", 32);
            m_odometryPublisher = m_nodeHandle.advertise<nav_msgs::Odometry>("/odom", 32);

            m_transBase2Odom.setIdentity();
            m_transKeyframe2Odom.setIdentity();
            m_timerBroadcast = m_nodeHandle.createWallTimer(ros::WallDuration(0.2), &CloudFilteringAndMatchingNodelet::broadcastTransforms, this);

            setDownsamplingMethod();
            setOutlierRejectionMethod();
            selectRegistrationMethod();

            m_bFirstCloud = true;
        }
    private:
        void setDownsamplingMethod() {
            std::string sDownsamplingMethod = m_privNodeHandle.param<std::string>("downsample_method", "VOXELGRID");
            double dDownsamplingResolution = m_privNodeHandle.param<double>("downsample_resolution", 0.1);

            if (sDownsamplingMethod == "VOXELGRID") {
                boost::shared_ptr<pcl::VoxelGrid<PointT>> voxelGrid(new pcl::VoxelGrid<PointT>());
                voxelGrid->setLeafSize(dDownsamplingResolution, dDownsamplingResolution, dDownsamplingResolution);
                m_downsamplingFilter = voxelGrid;
            } else if (sDownsamplingMethod == "APPROX_VOXELGRID") {
                boost::shared_ptr<pcl::ApproximateVoxelGrid<PointT>> approximateVoxelGrid(new pcl::ApproximateVoxelGrid<PointT>());
                approximateVoxelGrid->setLeafSize(dDownsamplingResolution, dDownsamplingResolution, dDownsamplingResolution);
                m_downsamplingFilter = approximateVoxelGrid;
            } else {
                boost::shared_ptr<pcl::PassThrough<PointT>> passthrough(new pcl::PassThrough<PointT>());
                m_downsamplingFilter = passthrough;
            }
        }

        void setOutlierRejectionMethod() {
            std::string sOutlierRejectionMethod = m_privNodeHandle.param<std::string>("outlier_rejection_method", "STATISTICAL");

            if (sOutlierRejectionMethod == "STATISTICAL") {
                int iMeanK = m_privNodeHandle.param<int>("or_stat_mean_k", 20);
                double dStddevMultiplierThreshold = m_privNodeHandle.param<double>("or_stat_stddev", 1.0);

                pcl::StatisticalOutlierRemoval<PointT>::Ptr statisticalOutlierRejection(new pcl::StatisticalOutlierRemoval<PointT>());
                statisticalOutlierRejection->setMeanK(iMeanK);
                statisticalOutlierRejection->setStddevMulThresh(dStddevMultiplierThreshold);
                m_outlierRejectionFilter = statisticalOutlierRejection;
            } else if (sOutlierRejectionMethod == "RADIUS") {
                double dRadius = m_privNodeHandle.param<double>("or_rad_radius", 0.8);
                int iMinNeighbours = m_privNodeHandle.param<int>("or_rad_min_neighbours", 2);

                pcl::RadiusOutlierRemoval<PointT>::Ptr radiusOutlierRejection(new pcl::RadiusOutlierRemoval<PointT>());
                radiusOutlierRejection->setRadiusSearch(dRadius);
                radiusOutlierRejection->setMinNeighborsInRadius(iMinNeighbours);
                m_outlierRejectionFilter = radiusOutlierRejection;
            } else {
                boost::shared_ptr<pcl::PassThrough<PointT>> passthrough(new pcl::PassThrough<PointT>());
                m_downsamplingFilter = passthrough;
            }
        }

        void selectRegistrationMethod() {
            std::string sRegistrationMethod = m_privNodeHandle.param<std::string>("registration_method", "NDT_OMP");

            if (sRegistrationMethod.find("ICP") != std::string::npos) {
                boost::shared_ptr<pclomp::GeneralizedIterativeClosestPoint<PointT, PointT>> gicp(new pclomp::GeneralizedIterativeClosestPoint<PointT, PointT>());
                m_registrationMethod = gicp;
                return;
            }

            int numThreads = m_privNodeHandle.param<int>("ndt_num_threads", 0);
            std::string nearestNeighboursSearchMethod = m_privNodeHandle.param<std::string>("ndt_search_method", "DIRECT7");
            double ndtResolution = m_privNodeHandle.param<double>("ndt_resolution", 0.5);

            boost::shared_ptr<pclomp::NormalDistributionsTransform<PointT, PointT>> ndt(new pclomp::NormalDistributionsTransform<PointT, PointT>());
            
            if(numThreads > 0)
                ndt->setNumThreads(numThreads);

            ndt->setTransformationEpsilon(0.01);
            ndt->setMaximumIterations(64);
            ndt->setResolution(ndtResolution);

            if(nearestNeighboursSearchMethod.find("KDTREE") != std::string::npos) {
                ndt->setNeighborhoodSearchMethod(pclomp::KDTREE);
            } else if (nearestNeighboursSearchMethod.find("DIRECT1") != std::string::npos) {
                ndt->setNeighborhoodSearchMethod(pclomp::DIRECT1);
            } else {
                ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
            }
            
            m_registrationMethod = ndt;
        }

        CloudT::ConstPtr transformCloudToBaseFrame(const CloudT::ConstPtr& sourceCloud) const {
            if (m_sBaseFrame.empty()) return sourceCloud;
            if (!m_tfListener.canTransform(m_sBaseFrame, sourceCloud->header.frame_id, ros::Time(0))) {
                std::cerr << "Unable to find transform between " << m_sBaseFrame << " and " << sourceCloud->header.frame_id << std::endl;
                return sourceCloud;
            }

            tf::StampedTransform transform;
            m_tfListener.waitForTransform(m_sBaseFrame, sourceCloud->header.frame_id, ros::Time(0), ros::Duration(2.0));
            m_tfListener.lookupTransform(m_sBaseFrame, sourceCloud->header.frame_id, ros::Time(0), transform);

            CloudT::Ptr transformedCloud(new CloudT());
            pcl_ros::transformPointCloud(*sourceCloud,*transformedCloud, transform);
            transformedCloud->header.frame_id = m_sBaseFrame;
            transformedCloud->header.stamp = sourceCloud->header.stamp;

            return transformedCloud;
        }

        CloudT::ConstPtr downsampleCloud(const CloudT::ConstPtr& cloud) const {
            if (!m_downsamplingFilter) return cloud;

            CloudT::Ptr filteredCloud(new CloudT());
            m_downsamplingFilter->setInputCloud(cloud);
            m_downsamplingFilter->filter(*filteredCloud);
            filteredCloud->header = cloud->header;

            return filteredCloud;
        }

        CloudT::ConstPtr rejectCloudOutliers(const CloudT::ConstPtr& cloud) const {
            if (!m_outlierRejectionFilter) return cloud;

            CloudT::Ptr filteredCloud(new CloudT());
            m_outlierRejectionFilter->setInputCloud(cloud);
            m_outlierRejectionFilter->filter(*filteredCloud);
            filteredCloud->header = cloud->header;

            return filteredCloud;
        }

        void cloudCallback(const sensor_msgs::PointCloud2ConstPtr& _cloudMessage) {
            CloudT::Ptr sourceCloud(new CloudT());
            pcl::fromROSMsg(*_cloudMessage, *sourceCloud);

            const auto stamp = _cloudMessage->header.stamp;

            auto transformedCloud = transformCloudToBaseFrame(sourceCloud);
            auto filteredCloud = downsampleCloud(transformedCloud);
            filteredCloud = rejectCloudOutliers(filteredCloud);

            m_filteredCloudPublisher.publish(filteredCloud);

            const auto odomPose = updateOdometry(stamp, filteredCloud);
            m_transBase2Odom = odomPose.matrix().cast<float>();

            auto odomMessage = createOdomMessageFromPose(stamp, odomPose);
            m_odometryPublisher.publish(odomMessage);
        }

        void broadcastTransforms(const ros::WallTimerEvent& event) {
            geometry_msgs::TransformStamped tsBase2Odom = createTransformFromMatrix(ros::Time::now(), m_transBase2Odom, m_sOdomFrame, m_sBaseFrame);
            m_transBase2OdomBroadcaster.sendTransform(tsBase2Odom);
            geometry_msgs::TransformStamped tsKey2Odom = createTransformFromMatrix(ros::Time::now(), m_transKeyframe2Odom, m_sOdomFrame, "keyframe");
            m_transKey2OdomBroadcaster.sendTransform(tsKey2Odom);
        }
        

        nav_msgs::Odometry createOdomMessageFromPose(const ros::Time& _stamp, const Eigen::Isometry3d& _pose) {
            nav_msgs::Odometry odomMessage;

            odomMessage.header.stamp = _stamp;
            odomMessage.header.frame_id = m_sOdomFrame;
            odomMessage.child_frame_id = m_sBaseFrame;

            Eigen::Quaterniond quat(_pose.linear());
            quat.norm();

            odomMessage.pose.pose.orientation.w = quat.w();
            odomMessage.pose.pose.orientation.x = quat.x();
            odomMessage.pose.pose.orientation.y = quat.y();
            odomMessage.pose.pose.orientation.z = quat.z();

            odomMessage.pose.pose.position.x = _pose.translation().x();
            odomMessage.pose.pose.position.y = _pose.translation().y();
            odomMessage.pose.pose.position.z = _pose.translation().z();

            odomMessage.twist.twist.angular.x = 
            odomMessage.twist.twist.angular.y = 
            odomMessage.twist.twist.angular.z = 0.0;
            odomMessage.twist.twist.linear.x = 
            odomMessage.twist.twist.linear.y = 
            odomMessage.twist.twist.linear.z = 0.0;

            return odomMessage;
        }

        Eigen::Isometry3d updateOdometry(const ros::Time& stamp, CloudT::ConstPtr& sourceCloud) {           
            if (!m_keyframe) {
                m_transformToKeyframe.setIdentity();

                m_keyframe = std::make_shared<Keyframe>(stamp, Eigen::Isometry3d::Identity(), 0, sourceCloud);
                m_registrationMethod->setInputTarget(m_keyframe->cloud);

                return Eigen::Isometry3d::Identity();
            }

            m_registrationMethod->setInputSource(sourceCloud);

            CloudT::Ptr alignedCloud(new CloudT());
            m_registrationMethod->align(*alignedCloud, m_transformToKeyframe.matrix().cast<float>());

            if (!m_registrationMethod->hasConverged())
                return m_keyframe->pose * m_transformToKeyframe;

            Eigen::Isometry3d matchedTransformation(m_registrationMethod->getFinalTransformation().cast<double>());

            // Check if difference between last known good transformation and current one is acceptably small
            if (m_bTransformationThresholding) {
                Eigen::Isometry3d delta = m_transformToKeyframe.inverse() * matchedTransformation;
                double translationDelta = delta.translation().norm();
                double angleDelta = std::acos(Eigen::Quaterniond(delta.linear()).w());

                if(translationDelta > m_dMaxTranslationDelta || angleDelta > m_dMaxAngularDelta)
                    return m_keyframe->pose * m_transformToKeyframe; // Return interpolated odometry
            }

            // Update odometry
            m_transformToKeyframe = matchedTransformation;
            m_odomPose = m_keyframe->pose * m_transformToKeyframe;

            // Broadcast transform between keyframe and odom frame.
            m_transKeyframe2Odom = m_keyframe->pose.matrix().cast<float>();

            updateKeyframe(stamp, sourceCloud);

            return m_odomPose;
        }

        void updateKeyframe(const ros::Time& stamp, CloudT::ConstPtr& sourceCloud) {
            Eigen::Isometry3d delta = m_transformToKeyframe;
            double translationDelta = delta.translation().norm();
            double angleDelta = std::acos(Eigen::Quaterniond(delta.linear()).w());
            double timeDelta = (stamp - m_keyframe->stamp).toSec();

            if (translationDelta > m_dMinKeyframeTranslationDelta || angleDelta > m_dMinKeyframeAngularDelta || timeDelta > m_dMinKeyframeTimeDelta) {
                m_keyframe->stamp = stamp;
                m_keyframe->pose = m_odomPose;
                m_keyframe->cloud = sourceCloud;

                m_registrationMethod->setInputTarget(m_keyframe->cloud);
                m_transformToKeyframe.setIdentity();
            }
        }

        // Nodehandles
        ros::NodeHandle m_nodeHandle;
        ros::NodeHandle m_privNodeHandle;

        // ROS subs/pubs
        std::string m_sPointsTopic;
        ros::Subscriber m_lidarSubscriber;
        ros::Publisher m_filteredCloudPublisher;
        ros::Publisher m_odometryPublisher;

        // TF
        tf::TransformListener m_tfListener;
        tf::TransformBroadcaster m_transBase2OdomBroadcaster;
        tf::TransformBroadcaster m_transKey2OdomBroadcaster;

        Eigen::Matrix4f m_transBase2Odom;
        Eigen::Matrix4f m_transKeyframe2Odom;

        ros::WallTimer m_timerBroadcast;

        std::string m_sBaseFrame;
        std::string m_sOdomFrame;

        // Matching
        bool m_bFirstCloud;
        Keyframe::Ptr m_keyframe;
        Eigen::Isometry3d m_transformToKeyframe, m_odomPose;
        bool m_bTransformationThresholding;
        double m_dMaxTranslationDelta, m_dMaxAngularDelta;
        double m_dMinKeyframeTranslationDelta, m_dMinKeyframeAngularDelta, m_dMinKeyframeTimeDelta;

        // PCL Filters
        pcl::Filter<PointT>::Ptr m_downsamplingFilter;
        pcl::Filter<PointT>::Ptr m_outlierRejectionFilter;

        pcl::Registration<PointT, PointT>::Ptr m_registrationMethod;

    };
}

PLUGINLIB_EXPORT_CLASS(graph_slam::CloudFilteringAndMatchingNodelet, nodelet::Nodelet)