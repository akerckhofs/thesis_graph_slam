#include <GraphSlam/GpsHandler.hpp>

namespace graph_slam {

    GpsHandler::GpsHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler)
    :   m_mutexQueue(_mutex),
        m_keyframeHandler(_keyframeHandler),
        m_graphSlam(_graphSlam) {
            m_nmeaParser.reset(new hdl_graph_slam::NmeaSentenceParser());
            m_bGPSEnabled = _privateNodeHandle.param<bool>("gps_enabled", false);
    }

    GpsHandler::~GpsHandler() {
    }
    void GpsHandler::addNMEA(const nmea_msgs::SentenceConstPtr& nmeaMessage) {
        hdl_graph_slam::GPRMC gprmc = m_nmeaParser->parse(nmeaMessage->sentence);
        if(gprmc.status != 'A')
            return;

        geographic_msgs::GeoPointStampedPtr gpsMessage(new geographic_msgs::GeoPointStamped());
        gpsMessage->header = nmeaMessage->header;
        gpsMessage->position.latitude = gprmc.latitude;
        gpsMessage->position.longitude = gprmc.longitude;
        gpsMessage->position.altitude = NAN;

        addGPS(gpsMessage);
    }
    void GpsHandler::addNavSat(const sensor_msgs::NavSatFixConstPtr& navsatMessage) {
        geographic_msgs::GeoPointStampedPtr gpsMessage(new geographic_msgs::GeoPointStamped());
        gpsMessage->header = navsatMessage->header;
        gpsMessage->position.latitude = navsatMessage->latitude;
        gpsMessage->position.longitude = navsatMessage->longitude;
        gpsMessage->position.altitude = navsatMessage->altitude;

        addGPS(gpsMessage);
    }
    void GpsHandler::addGPS(const geographic_msgs::GeoPointStampedPtr& gpsMessage) {
        std::lock_guard<std::mutex> lock(m_mutexQueue);
        m_gpsQueue.push_back(gpsMessage);
    }

    bool GpsHandler::flushQueue() {
        std::lock_guard<std::mutex> lock(m_mutexQueue);

        if (!m_bGPSEnabled) return false;

        auto& vKeyframes = m_keyframeHandler->getKeyframes();

        if (vKeyframes.empty() || m_gpsQueue.empty()) return false;

        bool bUpdated = false;
        auto gpsIterator = m_gpsQueue.begin();

        for (auto& currentKeyframe : vKeyframes) {
            // Try next keyframe if current one is already updated with GPS data.
            if (currentKeyframe->utmCoordinate)
                continue;
            // Break if keyframe is older than latest GPS data.
            if (currentKeyframe->stamp > m_gpsQueue.back()->header.stamp) 
                break;
            // Try next keyframe if available GPS data is older for current keyframe.
            if (currentKeyframe->stamp < (*gpsIterator)->header.stamp) 
                continue;

            // Find GPS data closest to current keyframe.
            auto closestGPS = findClosestGPS(currentKeyframe->stamp, gpsIterator);
            gpsIterator = closestGPS;

            if (std::abs(((*closestGPS)->header.stamp - currentKeyframe->stamp).toSec()) > 0.2)
                continue;

            geodesy::UTMPoint utm;
            geodesy::fromMsg((*closestGPS)->position, utm);
            Eigen::Vector3d currentXYZCoords(utm.easting, utm.northing, utm.altitude);

            if (!m_UTMOrigin) {
                m_UTMOrigin = currentXYZCoords;
            }
            currentXYZCoords -= (*m_UTMOrigin);

            currentKeyframe->utmCoordinate = currentXYZCoords;
            m_graphSlam->addGPSEdge(currentKeyframe, currentXYZCoords);

            bUpdated = true;
        }

        auto remove_loc = std::upper_bound(m_gpsQueue.begin(), m_gpsQueue.end(), vKeyframes.back()->stamp,
            [=](const ros::Time& stamp, const geographic_msgs::GeoPointStampedConstPtr& geopoint) {
                return stamp < geopoint->header.stamp;
            }
        );
        m_gpsQueue.erase(m_gpsQueue.begin(), remove_loc);

        return bUpdated;
    }

    std::deque<geographic_msgs::GeoPointStampedConstPtr>::iterator GpsHandler::findClosestGPS(ros::Time& stamp, std::deque<geographic_msgs::GeoPointStampedConstPtr>::iterator it) {
        auto closestGPS = it;
        for (auto currentGPS = it; currentGPS != m_gpsQueue.end(); currentGPS++) {

            auto closestTimeDelta = ((*closestGPS)->header.stamp - stamp).toSec();
            auto currentTimeDelta = ((*currentGPS)->header.stamp - stamp).toSec();

            if (std::abs(closestTimeDelta) < std::abs(currentTimeDelta))
                break;

            closestGPS = currentGPS;
        }
        return closestGPS;
    }
}