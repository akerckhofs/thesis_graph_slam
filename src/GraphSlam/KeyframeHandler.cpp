#include <GraphSlam/KeyframeHandler.hpp>

namespace graph_slam {
KeyframeHandler::KeyframeHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam)
    :   m_bFirstKeyframe(true),
        m_dAccumDistance(0),
        m_mutexQueue(_mutex),
        m_graphSlam(_graphSlam),
        m_prevKeyPose(Eigen::Isometry3d::Identity()) {
            minKeyframeTranslationDelta = _privateNodeHandle.param<double>("keyframe_delta_trans", 2.0);
            minKeyframeAngleDelta = _privateNodeHandle.param<double>("keyframe_delta_trans", 2.0);
            m_iKeyframeUpdateLimit = _privateNodeHandle.param<int>("max_keyframes_per_update", 10);
            m_transfOdom2Map.setIdentity();
}

KeyframeHandler::~KeyframeHandler() {
}

void KeyframeHandler::add(const ros::Time& stamp, const Eigen::Isometry3d& pose, const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud) {
    std::lock_guard<std::mutex> lock(m_mutexQueue);
    if(isKeyframe(stamp, pose, cloud)) {
        std::cout << "[KFH] Adding keyframe." << std::endl;
        Keyframe::Ptr keyframe(new Keyframe(stamp, pose, m_dAccumDistance, cloud));
        m_keyframeQueue.push_back(keyframe);
    }
}

bool KeyframeHandler::isKeyframe(const ros::Time& pStamp, const Eigen::Isometry3d& pPose, const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud) {
    using PointT = pcl::PointXYZI;
    using CloudT = pcl::PointCloud<PointT>;

    if(m_bFirstKeyframe) {
      m_bFirstKeyframe = false;
      m_prevKeyPose = pPose;
      return true;
    }

    Eigen::Isometry3d poseChange = m_prevKeyPose.inverse() * pPose;
    double translationDelta = poseChange.translation().norm();
    double angleDelta = std::acos(Eigen::Quaterniond(poseChange.linear()).w());

    if(translationDelta < minKeyframeTranslationDelta && angleDelta < minKeyframeAngleDelta)
      return false; // Too close to previous keyframe

    m_dAccumDistance += translationDelta;
    m_prevKeyPose = pPose;

    return true;
}

bool KeyframeHandler::flushQueue() {
    std::lock_guard<std::mutex> lock(m_mutexQueue);
    if(m_keyframeQueue.empty()) {
        return false;
    }

    int num_processed = 0;
    for(int i = 0;i < std::min<int>(m_keyframeQueue.size(), m_iKeyframeUpdateLimit); i++) {
        num_processed = i;

        const auto& currentKeyframe = m_keyframeQueue[i];
        m_loopMatchingQueue.push_back(currentKeyframe);

        Eigen::Isometry3d pose = m_transfOdom2Map * currentKeyframe->pose;
        currentKeyframe->graphNode = m_graphSlam->addNode(pose);

        if (m_vKeyframes.empty() && m_loopMatchingQueue.size() == 1) {
            m_graphSlam->setAnchorNode(currentKeyframe->graphNode);
        }

        if(i == 0 && m_vKeyframes.empty())
            continue;

        // If i == 0: take last added keyframe to vector
        // else take previous keyframe from queue
        const auto& prevKeyframe = i == 0 ? m_vKeyframes.back() : m_keyframeQueue[i - 1];
        m_graphSlam->addRelativePoseEdge(currentKeyframe, prevKeyframe);

    }

    m_keyframeQueue.erase(m_keyframeQueue.begin(), m_keyframeQueue.begin() + num_processed + 1);

    return true;
}

void KeyframeHandler::setOdom2MapTransform(const Eigen::Matrix4f& _transform) {
    m_transfOdom2Map = Eigen::Isometry3d(_transform.cast<double>());
}

void KeyframeHandler::pushLoopClosureKeyframes() {
    std::copy(m_loopMatchingQueue.begin(), m_loopMatchingQueue.end(), std::back_inserter(m_vKeyframes));
    m_loopMatchingQueue.clear();
}


}