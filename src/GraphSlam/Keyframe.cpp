#include <GraphSlam/Keyframe.hpp>
#include <g2o/types/slam3d/vertex_se3.h>

namespace graph_slam {

Keyframe::Keyframe(const ros::Time& _stamp, const Eigen::Isometry3d& _pose, double _accDistance, const CloudT::ConstPtr& _cloud)
  : stamp(_stamp),
    pose(_pose),
    accDistance(_accDistance),
    cloud(_cloud),
    graphNode(nullptr) {
}

KeyframeSnapshot::KeyframeSnapshot(const Eigen::Isometry3d& _pose, const CloudT::ConstPtr& _cloud)
  : pose(_pose),
    cloud(_cloud) {
}

KeyframeSnapshot::KeyframeSnapshot(const Keyframe::Ptr& _keyframe)
  : pose(_keyframe->graphNode->estimate()),
    cloud(_keyframe->cloud) {
}
}
