#include <GraphSlam/GraphSLAM.hpp>

#include <boost/format.hpp>
#include <g2o/stuff/macros.h>
#include <g2o/core/factory.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/linear_solver.h>
#include <g2o/core/sparse_optimizer.h>
#include <g2o/core/robust_kernel_factory.h>
#include <g2o/core/optimization_algorithm_factory.h>
#include <g2o/solvers/pcg/linear_solver_pcg.h>
#include <g2o/types/slam3d/types_slam3d.h>
#include <g2o/types/slam3d/edge_se3_pointxyz.h>
#include <g2o/types/slam3d_addons/types_slam3d_addons.h>
#include <g2o/edge_se3_priorxy.hpp>
#include <g2o/edge_se3_priorxyz.hpp>
#include <g2o/edge_se3_priorvec.hpp>
#include <g2o/edge_se3_priorquat.hpp>

G2O_USE_OPTIMIZATION_LIBRARY(pcg)
G2O_USE_OPTIMIZATION_LIBRARY(cholmod)
G2O_USE_OPTIMIZATION_LIBRARY(csparse)

namespace g2o {
  G2O_REGISTER_TYPE(EDGE_SE3_PRIORXY, EdgeSE3PriorXY)
  G2O_REGISTER_TYPE(EDGE_SE3_PRIORXYZ, EdgeSE3PriorXYZ)
  G2O_REGISTER_TYPE(EDGE_SE3_PRIORVEC, EdgeSE3PriorVec)
  G2O_REGISTER_TYPE(EDGE_SE3_PRIORQUAT, EdgeSE3PriorQuat)
}

namespace graph_slam {
    GraphSLAM::GraphSLAM(ros::NodeHandle& pPrivNodeHandle) {
        m_sOdoEdgeRobustKernel = pPrivNodeHandle.param<std::string>("odometry_edge_robust_kernel", "NONE");
        m_dOdoEdgeRobustKernelSize = pPrivNodeHandle.param<double>("odometry_edge_robust_kernel_size", 1.0);
        m_sGPSEdgeRobustKernel = pPrivNodeHandle.param<std::string>("gps_edge_robust_kernel", "");
        m_dGPSEdgeRobustKernelSize = pPrivNodeHandle.param<double>("gps_edge_robust_kernel_size", 1.0);
        m_dGPSEdgeStddevXY = pPrivNodeHandle.param<double>("gps_edge_stddev_xy", 10000.0);
        m_dGPSEdgeStddevZ = pPrivNodeHandle.param<double>("gps_edge_stddev_z", 10.0);
        m_sIMUEdgeRobustKernel = pPrivNodeHandle.param<std::string>("imu_edge_robust_kernel", "");
        m_dIMUEdgeRobustKernelSize = pPrivNodeHandle.param<double>("imu_edge_robust_kernel_size", 1.0);
        m_dIMUAccelerationStddev = pPrivNodeHandle.param<double>("imu_orientation_edge_stddev", 0.1);
        m_dIMUOrientationStddev = pPrivNodeHandle.param<double>("imu_acceleration_edge_stddev", 3.0);
        m_sLoopClosureEdgeRobustKernel = pPrivNodeHandle.param<std::string>("loop_closure_edge_robust_kernel", "NONE");
        m_dLoopClosureEdgeRobustKernelSize = pPrivNodeHandle.param<double>("loop_closure_edge_robust_kernel_size", 1.0);

        m_iOptimizationIterations = pPrivNodeHandle.param<int>("g2o_solver_num_iterations", 1024);
        m_sG2OSolverType = pPrivNodeHandle.param<std::string>("g2o_solver_type", "lm_var");

        m_anchorNode = nullptr;
        m_anchorEdge = nullptr;

        m_infoMatrixCalc.reset(new hdl_graph_slam::InformationMatrixCalculator(pPrivNodeHandle));

        setupGraph();

        m_robustKernelFactory = g2o::RobustKernelFactory::instance();
    }

    GraphSLAM::~GraphSLAM() {
        m_anchorNode = nullptr;
        m_anchorEdge = nullptr;

        m_infoMatrixCalc.reset();
        m_graph.reset();
    }

    void GraphSLAM::setupGraph() {
        m_graph.reset(new g2o::SparseOptimizer());

        g2o::OptimizationAlgorithmFactory* solverFactory = g2o::OptimizationAlgorithmFactory::instance();
        g2o::OptimizationAlgorithmProperty solverProperty;
        g2o::OptimizationAlgorithm* solver = solverFactory->construct(m_sG2OSolverType, solverProperty);
        m_graph->setAlgorithm(solver);

        if (!m_graph->solver()) {
            std::cout << "[GraphSLAM] Failed to construct solver." << std::endl;
            return;
        }

        std::cout << "[GraphSLAM] Created graph." << std::endl;
    }

    g2o::VertexSE3* GraphSLAM::setAnchorNode(g2o::VertexSE3* _node) {
        m_anchorNode = addSE3Node(Eigen::Isometry3d::Identity());
        m_anchorNode->setFixed(true);
        m_anchorEdge = addSE3Edge(m_anchorNode, _node, Eigen::Isometry3d::Identity(), Eigen::MatrixXd::Identity(6, 6));
    }

    g2o::VertexSE3* GraphSLAM::addNode(Eigen::Isometry3d& pose) {
        auto node = addSE3Node(pose);
        std::cout << "[GraphSLAM] Added keyframe node." << std::endl;
        return node;
    }

    void GraphSLAM::addRelativePoseEdge(const Keyframe::Ptr& pKeyframeNew, const Keyframe::Ptr& pKeyframeOld) {
        Eigen::Isometry3d relativePose = pKeyframeNew->pose.inverse() * pKeyframeOld->pose;
        Eigen::MatrixXd invCovarMatrix = m_infoMatrixCalc->calc_information_matrix(pKeyframeOld->cloud, pKeyframeNew->cloud, relativePose);
        auto edge = addSE3Edge(pKeyframeNew->graphNode, pKeyframeOld->graphNode, relativePose, invCovarMatrix);
        addRobustKernel(edge, m_sOdoEdgeRobustKernel, m_dOdoEdgeRobustKernelSize);
        std::cout << "[GraphSLAM] Added odom edge." << std::endl;
    }

    void GraphSLAM::addLoopEdge(const Loop::Ptr& _loop) {
        Eigen::Isometry3d relativePose(_loop->relativePose.cast<double>());

        Eigen::MatrixXd invCovarMatrix = m_infoMatrixCalc->calc_information_matrix(_loop->keyframeEnd->cloud, _loop->keyframeBegin->cloud, relativePose);
        auto edge = addSE3Edge(_loop->keyframeEnd->graphNode, _loop->keyframeBegin->graphNode, relativePose, invCovarMatrix);
        addRobustKernel(edge, m_sLoopClosureEdgeRobustKernel, m_dLoopClosureEdgeRobustKernelSize);
        std::cout << "[GraphSLAM] Added loop edge." << std::endl;
    }

    void GraphSLAM::addGPSEdge(const Keyframe::Ptr& pKeyframe, const Eigen::Vector3d xyzCoords) {
        g2o::OptimizableGraph::Edge* edge;

        if(std::isnan(xyzCoords.z())) {
            Eigen::Matrix2d invCovarMatrix = Eigen::Matrix2d::Identity() / m_dGPSEdgeStddevXY;
            edge = addSE3Prior2DEdge(pKeyframe->graphNode, xyzCoords.head<2>(), invCovarMatrix);
        } else {
            Eigen::Matrix3d invCovarMatrix = Eigen::Matrix3d::Identity();
            invCovarMatrix.block<2, 2>(0, 0) /= m_dGPSEdgeStddevXY;
            invCovarMatrix(2, 2) /= m_dGPSEdgeStddevZ;
            edge = addSE3Prior3DEdge(pKeyframe->graphNode, xyzCoords, invCovarMatrix);
        }
        addRobustKernel(edge, m_sGPSEdgeRobustKernel, m_dGPSEdgeRobustKernelSize);
        std::cout << "[GraphSLAM] Added GPS edge." << std::endl;
    }

    void GraphSLAM::addIMUOrientationEdge(const Keyframe::Ptr& pKeyframe) {
        Eigen::MatrixXd invCovarMatrix = Eigen::MatrixXd::Identity(3,3) / m_dIMUOrientationStddev;
        auto edge = addSE3PriorQuat(pKeyframe->graphNode, *pKeyframe->orientation, invCovarMatrix);
        addRobustKernel(edge, m_sIMUEdgeRobustKernel, m_dIMUEdgeRobustKernelSize);
        std::cout << "[GraphSLAM] Added IMU ori edge." << std::endl;
    }

    void GraphSLAM::addIMUAccelerationEdge(const Keyframe::Ptr& pKeyframe) {
        Eigen::MatrixXd invCovarMatrix = Eigen::MatrixXd::Identity(3,3) / m_dIMUAccelerationStddev;
        auto edge = addSE3PriorVec(pKeyframe->graphNode, Eigen::Vector3d::UnitZ(), *pKeyframe->acceleration, invCovarMatrix);
        addRobustKernel(edge, m_sIMUEdgeRobustKernel, m_dIMUEdgeRobustKernelSize);
        std::cout << "[GraphSLAM] Added IMU acc edge." << std::endl;
    }

    void GraphSLAM::optimizeGraph() {
        if (!m_graph) {
            std::cerr << "ERROR: tried to optimize uninitialized graph" << std::endl;
            return;
        }
        
        std::cout << "[GraphSLAM] Optimize called with " << m_graph->edges().size() << " edges." << std::endl;
        if (m_graph->edges().size() < 10) {
            return;
        }

        m_graph->initializeOptimization();
        m_graph->computeInitialGuess();
        m_graph->computeActiveErrors();

        auto iterations = m_graph->optimize(m_iOptimizationIterations);
    }

    g2o::VertexSE3* GraphSLAM::addSE3Node(const Eigen::Isometry3d& _pose) {
        g2o::VertexSE3* vertex(new g2o::VertexSE3());

        vertex->setId(static_cast<int>(m_graph->vertices().size()));
        vertex->setEstimate(_pose);
        m_graph->addVertex(vertex);

        return vertex;
    }

    g2o::EdgeSE3* GraphSLAM::addSE3Edge(g2o::VertexSE3* _node1, g2o::VertexSE3* _node2, const Eigen::Isometry3d& _relativePose, const Eigen::MatrixXd& _invCovarianceMatrix) {
        g2o::EdgeSE3* edge(new g2o::EdgeSE3());

        edge->setMeasurement(_relativePose);
        edge->setInformation(_invCovarianceMatrix);
        edge->vertices()[0] = _node1;
        edge->vertices()[1] = _node2;
        m_graph->addEdge(edge);

        return edge; 
    }

    g2o::EdgeSE3PriorXY* GraphSLAM::addSE3Prior2DEdge(g2o::VertexSE3* _node, const Eigen::Vector2d& _pos, const Eigen::MatrixXd& _invCovarianceMatrix) {
        g2o::EdgeSE3PriorXY* edge(new g2o::EdgeSE3PriorXY());

        edge->setMeasurement(_pos);
        edge->setInformation(_invCovarianceMatrix);
        edge->vertices()[0] = _node;
        m_graph->addEdge(edge);

        return edge;
    }

    g2o::EdgeSE3PriorXYZ* GraphSLAM::addSE3Prior3DEdge(g2o::VertexSE3* _node, const Eigen::Vector3d& _pos, const Eigen::MatrixXd& _invCovarianceMatrix) {
        g2o::EdgeSE3PriorXYZ* edge(new g2o::EdgeSE3PriorXYZ());

        edge->setMeasurement(_pos);
        edge->setInformation(_invCovarianceMatrix);
        edge->vertices()[0] = _node;
        m_graph->addEdge(edge);

        return edge;
    }

    g2o::EdgeSE3PriorVec* GraphSLAM::addSE3PriorVec(g2o::VertexSE3* _node, const Eigen::Vector3d& _direction, const Eigen::Vector3d& _measurement, const Eigen::MatrixXd& _invCovarianceMatrix) {
        g2o::EdgeSE3PriorVec* edge(new g2o::EdgeSE3PriorVec());

        Eigen::Matrix<double, 6, 1> m;
        m.head<3>() = _direction;
        m.tail<3>() = _measurement;

        edge->setMeasurement(m);
        edge->setInformation(_invCovarianceMatrix);
        edge->vertices()[0] = _node;
        m_graph->addEdge(edge);

        return edge;
    }



    g2o::EdgeSE3PriorQuat* GraphSLAM::addSE3PriorQuat(g2o::VertexSE3* _node, const Eigen::Quaterniond& _quat, const Eigen::MatrixXd& _invCovarianceMatrix) {
        g2o::EdgeSE3PriorQuat* edge(new g2o::EdgeSE3PriorQuat());

        edge->setMeasurement(_quat);
        edge->setInformation(_invCovarianceMatrix);
        edge->vertices()[0] = _node;
        m_graph->addEdge(edge);

        return edge;
    }

    void GraphSLAM::addRobustKernel(g2o::OptimizableGraph::Edge* _edge, const std::string _kernelType, double _kernelSize) {
        if(_kernelType == "NONE")
            return;

        g2o::RobustKernel* kernel = m_robustKernelFactory->construct(_kernelType);
        if(!kernel)
            return;

        kernel->setDelta(_kernelSize);
        _edge->setRobustKernel(kernel); 
    }
}