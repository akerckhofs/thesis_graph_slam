#include <GraphSlam/ImuHandler.hpp>

namespace graph_slam {
    ImuHandler::ImuHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler)
    :   m_mutexQueue(_mutex),
        m_keyframeHandler(_keyframeHandler),
        m_graphSlam(_graphSlam) {
            m_bAccelerationEnabled = _privateNodeHandle.param<bool>("enable_imu_acceleration", false);
            m_bOrientationEnabled = _privateNodeHandle.param<bool>("enable_imu_orientation", false);
    }

    ImuHandler::~ImuHandler() {
    }

    void ImuHandler::add(const sensor_msgs::ImuPtr& imuMessage) {
        if(!m_bAccelerationEnabled && !m_bOrientationEnabled)
        return;

        std::lock_guard<std::mutex> lock(m_mutexQueue);
        m_imuQueue.push_back(imuMessage);
    }

    bool ImuHandler::flushQueue() {
        std::lock_guard<std::mutex> lock(m_mutexQueue);

        auto& vKeyframes = m_keyframeHandler->getKeyframes();

        if(vKeyframes.empty() || m_imuQueue.empty() || m_sBaseFrame.empty())
            return false;

        bool bUpdated = false;
        auto imuIterator = m_imuQueue.begin();

        for (auto& currentKeyframe : vKeyframes) {
            // Try next keyframe if current has both orientation and acceleration set.
            if (currentKeyframe->orientation && currentKeyframe->acceleration) 
                continue;
            // Break if keyframe is older than latest IMU data.
            if (currentKeyframe->stamp > m_imuQueue.back()->header.stamp) 
                break;
            // Try next keyframe if available IMU data is older for current keyframe.
            if (currentKeyframe->stamp < (*imuIterator)->header.stamp) 
                continue;
            // Find GPS data closest to current keyframe.
            auto closestIMU = findClosest(currentKeyframe->stamp, imuIterator);
            imuIterator = closestIMU;

            if (std::abs(((*closestIMU)->header.stamp - currentKeyframe->stamp).toSec()) > 0.2)
                continue;

            Eigen::Vector3d baseAccel;
            Eigen::Quaterniond baseQuat;
            if (!transformToBase((*closestIMU), baseAccel, baseQuat))
                return false;

            currentKeyframe->acceleration = baseAccel;
            currentKeyframe->orientation = baseQuat;

            if (currentKeyframe->orientation->w() < 0.0) 
                currentKeyframe->orientation->coeffs() = -currentKeyframe->orientation->coeffs();

            if (m_bAccelerationEnabled)
                m_graphSlam->addIMUAccelerationEdge(currentKeyframe);
            if (m_bOrientationEnabled)
                m_graphSlam->addIMUOrientationEdge(currentKeyframe);

            bUpdated = true;
        }
        auto remove_loc = std::upper_bound(m_imuQueue.begin(), m_imuQueue.end(), vKeyframes.back()->stamp,
            [=](const ros::Time& stamp, const sensor_msgs::ImuConstPtr& imu) {
                return stamp < imu->header.stamp;
            }
        );
        m_imuQueue.erase(m_imuQueue.begin(), remove_loc);
        return bUpdated;
    }

    std::deque<sensor_msgs::ImuConstPtr>::iterator ImuHandler::findClosest(ros::Time& stamp, std::deque<sensor_msgs::ImuConstPtr>::iterator it) {
        auto closestIMU = it;
        for (auto currentIMU = it; currentIMU != m_imuQueue.end(); currentIMU++) {

            auto closestTimeDelta = ((*closestIMU)->header.stamp - stamp).toSec();
            auto currentTimeDelta = ((*currentIMU)->header.stamp - stamp).toSec();

            if (std::abs(closestTimeDelta) < std::abs(currentTimeDelta))
                break;

            closestIMU = currentIMU;
        }
        return closestIMU;
    }

    bool ImuHandler::transformToBase(sensor_msgs::ImuConstPtr& imuMessage, Eigen::Vector3d& baseAccel, Eigen::Quaterniond& baseQuat) {
        geometry_msgs::Vector3Stamped imuAccelStamped;
        geometry_msgs::QuaternionStamped imuQuatStamped;
        geometry_msgs::Vector3Stamped baseAccelStamped;
        geometry_msgs::QuaternionStamped baseQuatStamped;

        //imuQuatStamped.header.frame_id = imuAccelStamped.header.frame_id = imuMessage->header.frame_id;
        imuQuatStamped.header.frame_id = imuAccelStamped.header.frame_id = "velodyne";
        imuQuatStamped.header.stamp = imuAccelStamped.header.stamp = ros::Time(0);
        imuAccelStamped.vector = imuMessage->linear_acceleration;
        imuQuatStamped.quaternion = imuMessage->orientation;

        try {
            if (m_bAccelerationEnabled) m_tfListener.transformVector(m_sBaseFrame, imuAccelStamped, baseAccelStamped);
            if (m_bOrientationEnabled) m_tfListener.transformQuaternion(m_sBaseFrame, imuQuatStamped, baseQuatStamped);
        } catch (std::exception& e) {
            std::cerr << "[IMU] Failed to find transform to base frame: " << e.what() << std::endl;
            return false;
        }

        baseAccel = Eigen::Vector3d(baseAccelStamped.vector.x, baseAccelStamped.vector.y, baseAccelStamped.vector.z);
        baseQuat = Eigen::Quaterniond(baseQuatStamped.quaternion.w, baseQuatStamped.quaternion.x, baseQuatStamped.quaternion.y, baseQuatStamped.quaternion.z);

        return true;
    }

}