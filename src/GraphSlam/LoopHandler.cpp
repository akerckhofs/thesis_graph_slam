#include <GraphSlam/LoopHandler.hpp>

namespace graph_slam {
    LoopHandler::LoopHandler(ros::NodeHandle& _privateNodeHandle, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler)
        :   m_graphSlam(_graphSlam),
            m_keyframeHandler(_keyframeHandler) {
                selectRegistrationMethod(_privateNodeHandle);

                m_dAccDistanceThreshold = _privateNodeHandle.param<double>("acc_distance_threshold", 8.0);
                m_dMinEuclidianDistanceThreshold = _privateNodeHandle.param<double>("distance_threshold", 5.0);
                m_dMinAccDistanceFromLastLoop = _privateNodeHandle.param<double>("min_acc_distance_last_loop", 5.0);

                m_dMaxFitnessScore = _privateNodeHandle.param<double>("fitness_score_threshold", 0.5);
                m_dMaxCorrespondenceRange = _privateNodeHandle.param<double>("fitness_score_max_range", std::numeric_limits<double>::max());
    }

    void LoopHandler::findLoopCandidates(const Keyframe::Ptr& _loopEndKeyframe) {
        m_vCandidates.clear();
        // too close to last loop
        if (accumulatedDistanceFromLastLoop(_loopEndKeyframe) < m_dMinAccDistanceFromLastLoop)
            return;

        for (const auto& currentKeyframe : m_keyframeHandler->getKeyframes()) {
            if (_loopEndKeyframe->accDistance - currentKeyframe->accDistance < m_dAccDistanceThreshold)
                continue;

            const auto& candidateLoopStartPosition = currentKeyframe->graphNode->estimate().translation();
            const auto& loopEndPosition = _loopEndKeyframe->graphNode->estimate().translation();

            // norm(X1Y1 - X2Y2)
            auto distanceBetweenKeyframes = (candidateLoopStartPosition.head<2>() - loopEndPosition.head<2>()).norm();

            // keyframes too far apart -> not a loop.
            if (distanceBetweenKeyframes > m_dMinEuclidianDistanceThreshold) 
                continue;

            m_vCandidates.push_back(currentKeyframe);
        }
    }

    double LoopHandler::accumulatedDistanceFromLastLoop(const Keyframe::Ptr& _loopEndKeyframe) {
        return _loopEndKeyframe->accDistance - m_dAccDistanceAtLastLoop;
    }

    const Loop::Ptr LoopHandler::registerCandidates(const Keyframe::Ptr& _loopEndKeyframe) {
        if (m_vCandidates.empty()) 
            return nullptr;

        Keyframe::Ptr loopStartKeyframe;
        Eigen::Matrix4f relativePose;
        double bestFitnessScore = std::numeric_limits<double>::max();

        m_registrationMethod->setInputTarget(_loopEndKeyframe->cloud);

        pcl::PointCloud<PointT>::Ptr temp(new pcl::PointCloud<PointT>());
        std::cout << "[LOOP] Matching " << m_vCandidates.size() << " candidates.." << std::endl;
        for (const auto& candidate : m_vCandidates) {
            m_registrationMethod->setInputSource(candidate->cloud);
            Eigen::Matrix4f transformGuess = (_loopEndKeyframe->graphNode->estimate().inverse() * candidate->graphNode->estimate())
                                                    .matrix().cast<float>();
            transformGuess(2,3) = 0; // Set Z = 0;


            m_registrationMethod->align(*temp, transformGuess);
            auto fitnessScore = m_registrationMethod->getFitnessScore(m_dMaxCorrespondenceRange);

            if (!m_registrationMethod->hasConverged() || fitnessScore > bestFitnessScore)
                continue;
            
            bestFitnessScore = fitnessScore;
            loopStartKeyframe = candidate;
            relativePose = m_registrationMethod->getFinalTransformation();
        }

        std::cout << "[LOOP] Best match found with score: " << bestFitnessScore << " / " << m_dMaxFitnessScore << std::endl;
        if (bestFitnessScore > m_dMaxFitnessScore)
            return nullptr;
        
        m_dAccDistanceAtLastLoop = loopStartKeyframe->accDistance;
        
        return std::make_shared<Loop>(_loopEndKeyframe, loopStartKeyframe, relativePose);
    }

    void LoopHandler::addLoopsToGraph() {
        for (const auto& currentKeyframe : m_keyframeHandler->getLoopMatchingQueue()) {
            findLoopCandidates(currentKeyframe);
            const auto loop = registerCandidates(currentKeyframe);

            if (loop)
                m_graphSlam->addLoopEdge(loop);
        }

        m_keyframeHandler->pushLoopClosureKeyframes();
    }

    void LoopHandler::selectRegistrationMethod(ros::NodeHandle& _privateNodeHandle) {
        std::string sRegistrationMethod = _privateNodeHandle.param<std::string>("registration_method", "NDT_OMP");

        if (sRegistrationMethod.find("ICP") != std::string::npos) {
            boost::shared_ptr<pclomp::GeneralizedIterativeClosestPoint<PointT, PointT>> gicp(new pclomp::GeneralizedIterativeClosestPoint<PointT, PointT>());
            m_registrationMethod = gicp;
            return;
        }

        int numThreads = _privateNodeHandle.param<int>("ndt_num_threads", 0);
        std::string nearestNeighboursSearchMethod = _privateNodeHandle.param<std::string>("ndt_search_method", "DIRECT7");
        double ndtResolution = _privateNodeHandle.param<double>("ndt_resolution", 0.5);

        boost::shared_ptr<pclomp::NormalDistributionsTransform<PointT, PointT>> ndt(new pclomp::NormalDistributionsTransform<PointT, PointT>());
        
        if(numThreads > 0)
            ndt->setNumThreads(numThreads);

        ndt->setTransformationEpsilon(0.01);
        ndt->setMaximumIterations(64);
        ndt->setResolution(ndtResolution);

        if(nearestNeighboursSearchMethod.find("KDTREE") != std::string::npos) {
            ndt->setNeighborhoodSearchMethod(pclomp::KDTREE);
        } else if (nearestNeighboursSearchMethod.find("DIRECT1") != std::string::npos) {
            ndt->setNeighborhoodSearchMethod(pclomp::DIRECT1);
        } else {
            ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
        }
        
        m_registrationMethod = ndt;
    }
}