#include <GraphSlam/MapBuilder.hpp>
#include <pcl/octree/octree_search.h>

namespace graph_slam {

pcl::PointCloud<MapBuilder::PointT>::Ptr MapBuilder::build(const std::vector<KeyframeSnapshot::Ptr>& _vSnapshots, double _dResolution) const {
  pcl::PointCloud<PointT>::Ptr sourceCloud(new pcl::PointCloud<PointT>());

  if(_vSnapshots.empty()) {
    std::cerr << "[MapBuilder] Snapshots empty." << std::endl;
    return sourceCloud;
  }

  sourceCloud->reserve(_vSnapshots.front()->cloud->size() * _vSnapshots.size());

  for(const auto& snapshot : _vSnapshots) {
    Eigen::Matrix4f pose = snapshot->pose.matrix().cast<float>();

    for(const auto& sourcePoint : snapshot->cloud->points) {
      PointT targetPoint;
      targetPoint.getVector4fMap() = pose * sourcePoint.getVector4fMap();
      targetPoint.intensity = sourcePoint.intensity;
      sourceCloud->push_back(targetPoint);
    }
  }

  sourceCloud->width = sourceCloud->size();
  sourceCloud->height = 1;
  sourceCloud->is_dense = false;

  return rebuildWithVoxelCenters(sourceCloud, _dResolution);
}

pcl::PointCloud<MapBuilder::PointT>::Ptr MapBuilder::rebuildWithVoxelCenters(pcl::PointCloud<PointT>::Ptr sourceCloud, double _dResolution) const {
  pcl::octree::OctreePointCloud<PointT> octree(_dResolution);
  octree.setInputCloud(sourceCloud);
  octree.addPointsFromInputCloud();

  pcl::PointCloud<PointT>::Ptr filteredCloud(new pcl::PointCloud<PointT>());
  octree.getOccupiedVoxelCenters(filteredCloud->points);

  filteredCloud->width = filteredCloud->size();
  filteredCloud->height = 1;
  filteredCloud->is_dense = false;

  return filteredCloud;
}

}
