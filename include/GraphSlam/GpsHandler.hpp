#ifndef GPS_HANDLER_HPP
#define GPS_HANDLER_HPP

#include <mutex>
#include <deque>
#include <unordered_map>
#include <boost/optional.hpp>

#include <ros/ros.h>

#include <nmea_msgs/Sentence.h>
#include <nmea_msgs/Gprmc.h>
#include <sensor_msgs/NavSatFix.h>
#include <geographic_msgs/GeoPointStamped.h>
#include <geodesy/utm.h>
#include <geodesy/wgs84.h>

#include <GraphSlam/GraphSLAM.hpp>
#include <GraphSlam/KeyframeHandler.hpp>
#include <nmea_sentence_parser.hpp>

namespace graph_slam {
class GpsHandler {
public:
    GpsHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler);
    ~GpsHandler();

    void addNMEA(const nmea_msgs::SentenceConstPtr& nmeaMessage);
    void addNavSat(const sensor_msgs::NavSatFixConstPtr& navsatMessage);
    void addGPS(const geographic_msgs::GeoPointStampedPtr& gps_msg);
    bool flushQueue();
private:
    std::deque<geographic_msgs::GeoPointStampedConstPtr>::iterator findClosestGPS(ros::Time& stamp, std::deque<geographic_msgs::GeoPointStampedConstPtr>::iterator it);
    boost::optional<Eigen::Vector3d> m_UTMOrigin;

    bool m_bGPSEnabled;

    std::mutex& m_mutexQueue;
    std::deque<geographic_msgs::GeoPointStampedConstPtr> m_gpsQueue;

    std::shared_ptr<GraphSLAM> m_graphSlam;
    std::unique_ptr<hdl_graph_slam::NmeaSentenceParser> m_nmeaParser;
    std::shared_ptr<KeyframeHandler> m_keyframeHandler;
};
}

#endif // GPS_HANDLER_HPP