#ifndef KEYFRAME_HANDLER_HPP
#define KEYFRAME_HANDLER_HPP

#include <mutex>
#include <deque>
#include <unordered_map>
#include <boost/optional.hpp>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <GraphSlam/Keyframe.hpp>
#include <GraphSlam/GraphSLAM.hpp>


namespace graph_slam {

class GraphSLAM;

class KeyframeHandler {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    KeyframeHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam);
    ~KeyframeHandler();

    void add(const ros::Time& stamp, const Eigen::Isometry3d& pose, const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud);
    bool flushQueue();
    void setOdom2MapTransform(const Eigen::Matrix4f& _transform);
    const std::vector<Keyframe::Ptr>& getKeyframes() {
        return m_vKeyframes;
    }

    const std::deque<Keyframe::Ptr>& getLoopMatchingQueue() {
        return m_loopMatchingQueue;
    }

    void pushLoopClosureKeyframes();
private:
    bool isKeyframe(const ros::Time& pStamp, const Eigen::Isometry3d& pPose, const pcl::PointCloud<pcl::PointXYZI>::ConstPtr& cloud);

    bool m_bFirstKeyframe;
    Eigen::Isometry3d m_prevKeyPose;
    double m_dAccumDistance;

    double minKeyframeAngleDelta;
    double minKeyframeTranslationDelta;
    int m_iKeyframeUpdateLimit;

    Eigen::Isometry3d m_transfOdom2Map;

    std::mutex& m_mutexQueue;
    std::deque<Keyframe::Ptr> m_keyframeQueue;
    std::deque<Keyframe::Ptr> m_loopMatchingQueue;
    std::vector<Keyframe::Ptr> m_vKeyframes;

    std::shared_ptr<GraphSLAM> m_graphSlam;
};
}

#endif // KEYFRAME_HANDLER_HPP