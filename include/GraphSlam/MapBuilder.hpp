#ifndef MAP_BUILDER_HPP
#define MAP_BUILDER_HPP

#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <GraphSlam/Keyframe.hpp>


namespace graph_slam {
class MapBuilder {
public:
  using PointT = pcl::PointXYZI;
  MapBuilder() {}
  ~MapBuilder() {}

  pcl::PointCloud<PointT>::Ptr build(const std::vector<KeyframeSnapshot::Ptr>& _vSnapshots, double _dResolution) const;
private:
  pcl::PointCloud<PointT>::Ptr rebuildWithVoxelCenters(pcl::PointCloud<PointT>::Ptr sourceCloud, double _dResolution) const;
};

}

#endif // MAP_BUILDER_HPP
