#ifndef SLAM_UTILS_HPP
#define SLAM_UTILS_HPP

#include <Eigen/Dense>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TransformStamped.h>



namespace graph_slam {
    static Eigen::Isometry3d extractPoseFromOdometryMessage(const nav_msgs::OdometryConstPtr& _odometryMsg) {
        Eigen::Isometry3d pose;
        const auto& orientation = _odometryMsg->pose.pose.orientation;
        const auto& position = _odometryMsg->pose.pose.position;

        Eigen::Quaterniond quat;

        quat.w() = orientation.w;
        quat.x() = orientation.x;
        quat.y() = orientation.y;
        quat.z() = orientation.z;

        pose.setIdentity();
        pose.translation() = Eigen::Vector3d(position.x, position.y, position.z);
        pose.linear() = quat.toRotationMatrix();

        return pose;
    }

    static geometry_msgs::TransformStamped createTransformFromMatrix(const ros::Time& _stamp, const Eigen::Matrix4f& _pose, const std::string& _toFrameId, const std::string& _fromFrameId) {
        geometry_msgs::TransformStamped transform;
        transform.header.stamp = _stamp;
        transform.header.frame_id = _toFrameId;
        transform.child_frame_id = _fromFrameId;
        
        Eigen::Quaternionf quat(_pose.block<3, 3>(0, 0));
        quat.normalize();
        transform.transform.rotation.w = quat.w();
        transform.transform.rotation.x = quat.x();
        transform.transform.rotation.y = quat.y();
        transform.transform.rotation.z = quat.z();

        transform.transform.translation.x = _pose(0, 3);
        transform.transform.translation.y = _pose(1, 3);
        transform.transform.translation.z = _pose(2, 3);

        return transform;
    }
}

#endif // SLAM_UTILS_HPP