#ifndef IMU_HANDLER_HPP
#define IMU_HANDLER_HPP

#include <mutex>
#include <deque>
#include <unordered_map>
#include <boost/optional.hpp>

#include <ros/ros.h>

#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/QuaternionStamped.h>

#include <GraphSlam/GraphSLAM.hpp>
#include <GraphSlam/KeyframeHandler.hpp>

namespace graph_slam {
class ImuHandler {
public:
    ImuHandler(ros::NodeHandle& _privateNodeHandle, std::mutex& _mutex, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler);
    ~ImuHandler();

    void add(const sensor_msgs::ImuPtr& imuMessage);
    bool flushQueue();
    void setBaseFrame(std::string _baseFrame) {
        m_sBaseFrame = _baseFrame;
    }
private:
    std::deque<sensor_msgs::ImuConstPtr>::iterator findClosest(ros::Time& stamp, std::deque<sensor_msgs::ImuConstPtr>::iterator it);
    bool transformToBase(sensor_msgs::ImuConstPtr& imuMessage, Eigen::Vector3d& baseAccel, Eigen::Quaterniond& baseQuat);

    bool m_bAccelerationEnabled, m_bOrientationEnabled;
    double imu_acceleration_edge_stddev;


    std::mutex& m_mutexQueue;
    std::deque<sensor_msgs::ImuConstPtr> m_imuQueue;

    std::shared_ptr<GraphSLAM> m_graphSlam;
    std::shared_ptr<KeyframeHandler> m_keyframeHandler;

    tf::TransformListener m_tfListener;
    std::string m_sBaseFrame;
    
};
}

#endif // IMU_HANDLER_HPP