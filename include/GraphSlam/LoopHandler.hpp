#ifndef LOOP_HANDLER_HPP
#define LOOP_HANDLER_HPP

#include <mutex>
#include <deque>
#include <unordered_map>
#include <boost/optional.hpp>
#include <boost/format.hpp>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <pcl/registration/registration.h>
#include <pclomp/ndt_omp.h>
#include <pclomp/gicp_omp.h>

#include <GraphSlam/Loop.hpp>
#include <GraphSlam/Keyframe.hpp>

#include <GraphSlam/KeyframeHandler.hpp>
#include <GraphSlam/GraphSLAM.hpp>
#include <g2o/types/slam3d/vertex_se3.h>


namespace graph_slam {

class LoopHandler {
public:
    using PointT = pcl::PointXYZI;

    LoopHandler(ros::NodeHandle& _privateNodeHandle, std::shared_ptr<GraphSLAM>& _graphSlam, std::shared_ptr<KeyframeHandler>& _keyframeHandler);
    ~LoopHandler() {}
    void addLoopsToGraph();
private:
    void findLoopCandidates(const Keyframe::Ptr& _loopEndKeyframe);
    const Loop::Ptr registerCandidates(const Keyframe::Ptr& _loopEndKeyframe);
    double accumulatedDistanceFromLastLoop(const Keyframe::Ptr& _loopEndKeyframe);
    void selectRegistrationMethod(ros::NodeHandle& _privateNodeHandle);

    std::vector<Keyframe::Ptr> m_vCandidates;
    std::vector<Loop::Ptr> m_vLoops;
    pcl::Registration<PointT, PointT>::Ptr m_registrationMethod;

    std::shared_ptr<GraphSLAM> m_graphSlam;
    std::shared_ptr<KeyframeHandler> m_keyframeHandler;

    double m_dMinAccDistanceFromLastLoop; // Minimum accumulated distance between keyframes with loop contraints.
    double m_dMinEuclidianDistanceThreshold; // Minimum Euclidian distance between start and end of a loop.
    double m_dAccDistanceThreshold; // Minimum accumulated distance between keyframes.

    double m_dAccDistanceAtLastLoop; // Accumulated distance at the last keyframe with a loop constraint.

    double m_dMaxCorrespondenceRange; // Max distance between corresponding points when matching. (Used to scale fitness score)
    double m_dMaxFitnessScore; // Maximum fitness score for a match.
    
};
}


#endif // LOOP_HANDLER_HPP