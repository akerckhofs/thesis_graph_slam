#ifndef KEYFRAME_HPP
#define KEYFRAME_HPP

#include <ros/ros.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <boost/optional.hpp>

namespace g2o {
  class VertexSE3;
}

namespace graph_slam {

struct Keyframe {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using PointT = pcl::PointXYZI;
  using CloudT = pcl::PointCloud<PointT>;
  using Ptr = std::shared_ptr<Keyframe>;

  Keyframe(const ros::Time& stamp, const Eigen::Isometry3d& pose, double accDistance, const CloudT::ConstPtr& cloud);
  ~Keyframe() {}

public:
  ros::Time stamp;
  Eigen::Isometry3d pose;
  double accDistance;
  pcl::PointCloud<PointT>::ConstPtr cloud;
  boost::optional<Eigen::Vector3d> utmCoordinate;

  boost::optional<Eigen::Vector3d> acceleration;
  boost::optional<Eigen::Quaterniond> orientation;

  g2o::VertexSE3* graphNode;
};

/**
 * @brief KeyFramesnapshot for map cloud generation
 */
struct KeyframeSnapshot {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using PointT = Keyframe::PointT;
  using CloudT = pcl::PointCloud<PointT>;
  using Ptr = std::shared_ptr<KeyframeSnapshot>;

  KeyframeSnapshot(const Keyframe::Ptr& _keyframe);
  KeyframeSnapshot(const Eigen::Isometry3d& _pose, const CloudT::ConstPtr& _cloud);

  ~KeyframeSnapshot() {}

public:
  Eigen::Isometry3d pose;                   // pose estimated by graph optimization
  CloudT::ConstPtr cloud;  // point cloud
};

}

#endif // KEYFRAME_HPP
