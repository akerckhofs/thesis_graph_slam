#ifndef GRAPH_SLAM_HPP
#define GRAPH_SLAM_HPP

#include <ros/ros.h>

#include <memory>
#include <ros/time.h>
#include <Eigen/Dense>
#include <GraphSlam/Keyframe.hpp>
#include <GraphSlam/Loop.hpp>

#include <g2o/core/sparse_optimizer.h>

#include <information_matrix_calculator.hpp>

namespace g2o {
  class VertexSE3;
  class VertexPlane;
  class VertexPointXYZ;
  class EdgeSE3;
  class EdgeSE3PointXYZ;
  class EdgeSE3PriorXY;
  class EdgeSE3PriorXYZ;
  class EdgeSE3PriorVec;
  class EdgeSE3PriorQuat;
  class RobustKernelFactory;
}

namespace graph_slam {

class GraphSLAM {
public:
    GraphSLAM(ros::NodeHandle& pPrivNodeHandle);
    ~GraphSLAM();

    void setupGraph();

    g2o::VertexSE3* setAnchorNode(g2o::VertexSE3* pNode);
    g2o::VertexSE3* addNode(Eigen::Isometry3d& pose);
    void addRelativePoseEdge(const Keyframe::Ptr& pKeyframeNew, const Keyframe::Ptr& pKeyframeOld);
    void addGPSEdge(const Keyframe::Ptr& pKeyframe, const Eigen::Vector3d xyzCoords);
    void addIMUOrientationEdge(const Keyframe::Ptr& pKeyframe);
    void addIMUAccelerationEdge(const Keyframe::Ptr& pKeyframe);
    void addLoopEdge(const Loop::Ptr& _loop);
    void optimizeGraph();
private:
    g2o::VertexSE3* addSE3Node(const Eigen::Isometry3d& _pose);
    g2o::EdgeSE3* addSE3Edge(g2o::VertexSE3* _node1, g2o::VertexSE3* _node2, const Eigen::Isometry3d& _relativePose, const Eigen::MatrixXd& _invCovarianceMatrix);
    g2o::EdgeSE3PriorXY* addSE3Prior2DEdge(g2o::VertexSE3* _node, const Eigen::Vector2d& _pos, const Eigen::MatrixXd& _invCovarianceMatrix);
    g2o::EdgeSE3PriorXYZ* addSE3Prior3DEdge(g2o::VertexSE3* _node, const Eigen::Vector3d& _pos, const Eigen::MatrixXd& _invCovarianceMatrix);
    g2o::EdgeSE3PriorVec* addSE3PriorVec(g2o::VertexSE3* _node, const Eigen::Vector3d& _direction, const Eigen::Vector3d& _measurement, const Eigen::MatrixXd& _invCovarianceMatrix);
    g2o::EdgeSE3PriorQuat* addSE3PriorQuat(g2o::VertexSE3* _node, const Eigen::Quaterniond& _quat, const Eigen::MatrixXd& _invCovarianceMatrix);
    void addRobustKernel(g2o::OptimizableGraph::Edge* _edge, const std::string _kernelType, double _kernelSize);

    // Odometry
    std::string m_sOdoEdgeRobustKernel;
    double m_dOdoEdgeRobustKernelSize;

    // GPS
    std::string m_sGPSEdgeRobustKernel;
    double m_dGPSEdgeRobustKernelSize;
    double m_dGPSEdgeStddevXY;
    double m_dGPSEdgeStddevZ;

    // IMU
    std::string m_sIMUEdgeRobustKernel;
    double m_dIMUEdgeRobustKernelSize;
    double m_dIMUOrientationStddev;
    double m_dIMUAccelerationStddev;

    // Loop closure
    std::string m_sLoopClosureEdgeRobustKernel;
    double m_dLoopClosureEdgeRobustKernelSize;

    // Graph
    std::unique_ptr<g2o::SparseOptimizer> m_graph;
    g2o::RobustKernelFactory* m_robustKernelFactory;
    
    std::string m_sG2OSolverType;

    int m_iOptimizationIterations;

    g2o::VertexSE3* m_anchorNode;
    g2o::EdgeSE3* m_anchorEdge;

    std::unique_ptr<hdl_graph_slam::InformationMatrixCalculator> m_infoMatrixCalc;
};
}

#endif // GRAPH_SLAM_HANDLER_HPP