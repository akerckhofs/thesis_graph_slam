#ifndef LOOP_HPP
#define LOOP_HPP

#include <Eigen/Dense>
#include <GraphSlam/Keyframe.hpp>

namespace graph_slam {
struct Loop {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    using Ptr = std::shared_ptr<Loop>;

    Loop(const Keyframe::Ptr& _keyframeEnd, const Keyframe::Ptr& _keyframeBegin, const Eigen::Matrix4f& _relativePose)
        :   keyframeBegin(_keyframeBegin),
            keyframeEnd(_keyframeEnd),
            relativePose(_relativePose) {

    }
    
    Keyframe::Ptr keyframeBegin;
    Keyframe::Ptr keyframeEnd;
    Eigen::Matrix4f relativePose;
};
}

#endif //LOOP_HPP

