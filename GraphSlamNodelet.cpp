#include <string>
#include <mutex>

#include <ros/ros.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <geodesy/utm.h>
#include <geodesy/wgs84.h>

#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

#include <nmea_msgs/Sentence.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>

#include <Eigen/Dense>

#include <pcl/point_cloud.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

#include <GraphSlam/Utils.hpp>

#include <GraphSlam/KeyframeHandler.hpp>
#include <GraphSlam/ImuHandler.hpp>
#include <GraphSlam/GpsHandler.hpp>
#include <GraphSlam/LoopHandler.hpp>
#include <GraphSlam/MapBuilder.hpp>
#include <GraphSlam/GraphSLAM.hpp>

namespace graph_slam {
    class GraphSlamNodelet : public nodelet::Nodelet {

    public:
        typedef pcl::PointXYZI PointT;
        typedef pcl::PointCloud<PointT> CloudT;

        GraphSlamNodelet() {}
        virtual ~GraphSlamNodelet() {}
        virtual void onInit() {
            m_nodeHandle = getNodeHandle();
            m_mtNodeHandle = getMTNodeHandle();
            m_privNodeHandle = getPrivateNodeHandle();

            m_sMapFrame = m_privNodeHandle.param<std::string>("map_frame", "map");
            m_sOdomFrame = m_privNodeHandle.param<std::string>("odom_frame", "odom");

            m_graphSlam = std::make_shared<GraphSLAM>(m_privNodeHandle);
            m_keyframeHandler = std::make_shared<KeyframeHandler>(m_privNodeHandle, m_mutexKeyframes, m_graphSlam);
            m_gpsHandler = boost::make_shared<GpsHandler>(m_privNodeHandle, m_mutexGPS, m_graphSlam, m_keyframeHandler);
            m_imuHandler = boost::make_shared<ImuHandler>(m_privNodeHandle, m_mutexImu, m_graphSlam, m_keyframeHandler);
            m_loopHandler.reset(new LoopHandler(m_privNodeHandle, m_graphSlam, m_keyframeHandler));

            m_dMapCloudResolution = m_privNodeHandle.param<double>("map_cloud_resolution", 0.05);
            m_mapBuilder.reset(new MapBuilder());

            m_odomSubscriber.reset(new message_filters::Subscriber<nav_msgs::Odometry>(m_mtNodeHandle, "/odom", 256));
            m_cloudSubscriber.reset(new message_filters::Subscriber<sensor_msgs::PointCloud2>(m_mtNodeHandle, "/filtered_points", 32));
            m_odomCloudSync.reset(new message_filters::TimeSynchronizer<nav_msgs::Odometry, sensor_msgs::PointCloud2>(*m_odomSubscriber, *m_cloudSubscriber, 32));
            m_odomCloudSync->registerCallback(boost::bind(&GraphSlamNodelet::keyframeCallback, this, _1, _2));

            m_sImuTopic = m_privNodeHandle.param<std::string>("imu_topic", "/gpsimu_driver/imu_data");
            m_imuSubscriber = m_mtNodeHandle.subscribe(m_sImuTopic, 1024, &ImuHandler::add, m_imuHandler); // TODO: Make topics variables

            m_sGPSTopic = m_privNodeHandle.param<std::string>("gps_topic", "/gps/geopoint");
            m_sNMEATopic = m_privNodeHandle.param<std::string>("nmea_topic", "/gpsimu_driver/nmea_sentence");
            m_sNavSatTopic = m_privNodeHandle.param<std::string>("navsat_topic", "/gps/navsat");

            m_gpsSubscriber = m_mtNodeHandle.subscribe(m_sGPSTopic, 1024, &GpsHandler::addGPS, m_gpsHandler);
            m_nmeaSubscriber = m_mtNodeHandle.subscribe(m_sNMEATopic, 1024, &GpsHandler::addNMEA, m_gpsHandler);
            m_navsatSubscriber = m_mtNodeHandle.subscribe(m_sNavSatTopic, 1024, &GpsHandler::addNavSat, m_gpsHandler);

            m_transOdom2Map.setIdentity();
            m_mapCloudPublisher = m_mtNodeHandle.advertise<sensor_msgs::PointCloud2>("/map_points", 1);

            m_dOptimizationInterval = m_privNodeHandle.param<double>("graph_update_interval", 3.0);
            m_dMapPublishInterval = m_privNodeHandle.param<double>("map_cloud_update_interval", 10.0);
            m_timerOptimization = m_mtNodeHandle.createWallTimer(ros::WallDuration(m_dOptimizationInterval), &GraphSlamNodelet::graphOptimizationCallback, this);
            m_timerMapPublish = m_mtNodeHandle.createWallTimer(ros::WallDuration(m_dMapPublishInterval), &GraphSlamNodelet::mapCloudPublishCallback, this);
            m_markersPublisher = m_privNodeHandle.advertise<visualization_msgs::MarkerArray>("/markers", 16);
            m_timerOdom2Map = m_mtNodeHandle.createWallTimer(ros::WallDuration(0.2), &GraphSlamNodelet::broadcastOdom2Map, this);
        }
    private:
        void keyframeCallback(const nav_msgs::OdometryConstPtr& _odometryMessage, const sensor_msgs::PointCloud2::ConstPtr& _cloudMessage) {
            CloudT::Ptr cloud(new CloudT());
            pcl::fromROSMsg(*_cloudMessage, *cloud);
            
            const ros::Time& stamp = _odometryMessage->header.stamp;
            auto const pose = extractPoseFromOdometryMessage(_odometryMessage);
            
            if (m_sBaseFrame.empty()) {
                m_sBaseFrame = _cloudMessage->header.frame_id; // Set base frame to lidar frame if it hasn't been set already.
                m_imuHandler->setBaseFrame(m_sBaseFrame);
            }
            m_keyframeHandler->add(stamp, pose, cloud);
        }

        void mapCloudPublishCallback(const ros::WallTimerEvent& event) {
            std::lock_guard<std::mutex> lock(m_mutexSnapshots);
            if (!m_mapCloudPublisher.getNumSubscribers())
                return;

            auto mapCloud = m_mapBuilder->build(m_vKeyframeSnapshots, m_dMapCloudResolution);
            if (mapCloud->empty())
                return;

            mapCloud->header.frame_id = m_sMapFrame;
            mapCloud->header.stamp = m_vKeyframeSnapshots.back()->cloud->header.stamp;

            sensor_msgs::PointCloud2Ptr mapCloudMessage(new sensor_msgs::PointCloud2());
            pcl::toROSMsg(*mapCloud, *mapCloudMessage);

            m_mapCloudPublisher.publish(mapCloudMessage);
        }

        void graphOptimizationCallback(const ros::WallTimerEvent& event) {
            std::lock_guard<std::mutex> lock(m_mutexMain);

            if (!flushQueues())
                return;

            m_loopHandler->addLoopsToGraph();

            // optimize the pose graph
            m_graphSlam->optimizeGraph();
            rebuildSnapshots();

            if(m_markersPublisher.getNumSubscribers()) {
                auto markers = create_marker_array(ros::Time::now());
                m_markersPublisher.publish(markers);
            }

            const auto& keyframe = m_keyframeHandler->getKeyframes().back();
            m_transOdom2Map = (keyframe->graphNode->estimate() * keyframe->pose.inverse()).matrix().cast<float>();

            m_keyframeHandler->setOdom2MapTransform(m_transOdom2Map);

            if (m_odom2mapPublisher.getNumSubscribers()) {
                geometry_msgs::TransformStamped ts = createTransformFromMatrix(keyframe->stamp, m_transOdom2Map, m_sMapFrame, m_sOdomFrame);
                m_odom2mapPublisher.publish(ts);
                
            }
        }

        void broadcastOdom2Map(const ros::WallTimerEvent& event) {
            geometry_msgs::TransformStamped ts = createTransformFromMatrix(ros::Time::now(), m_transOdom2Map, m_sMapFrame, m_sOdomFrame);
            m_transOdom2MapBroadcaster.sendTransform(ts);
        }

        void rebuildSnapshots() {
            std::vector<KeyframeSnapshot::Ptr> tempSnapshots;
            tempSnapshots.reserve(m_keyframeHandler->getKeyframes().size());

            for (const auto& currentKeyframe : m_keyframeHandler->getKeyframes()) {
                tempSnapshots.push_back(std::make_shared<KeyframeSnapshot>(currentKeyframe));
            }

            m_mutexSnapshots.lock();
            m_vKeyframeSnapshots.swap(tempSnapshots);
            m_mutexSnapshots.unlock();
        }

        bool flushQueues() {
            auto bKeyframesUpdated = m_keyframeHandler->flushQueue();
            auto bIMUUpdated = m_imuHandler->flushQueue();
            auto bGPSUpdated = m_gpsHandler->flushQueue();

            return (bKeyframesUpdated || bIMUUpdated || bGPSUpdated);
        }

        visualization_msgs::MarkerArray create_marker_array(const ros::Time& stamp) const {
            auto keyframes = m_keyframeHandler->getKeyframes();
            visualization_msgs::MarkerArray markers;
            markers.markers.resize(5);

            // node markers
            visualization_msgs::Marker& traj_marker = markers.markers[0];
            traj_marker.header.frame_id = "map";
            traj_marker.header.stamp = stamp;
            traj_marker.ns = "nodes";
            traj_marker.id = 0;
            traj_marker.type = visualization_msgs::Marker::SPHERE_LIST;

            traj_marker.pose.orientation.w = 1.0;
            traj_marker.scale.x = traj_marker.scale.y = traj_marker.scale.z = 0.5;

            visualization_msgs::Marker& imu_marker = markers.markers[4];
            imu_marker.header = traj_marker.header;
            imu_marker.ns = "imu";
            imu_marker.id = 4;
            imu_marker.type = visualization_msgs::Marker::SPHERE_LIST;

            imu_marker.pose.orientation.w = 1.0;
            imu_marker.scale.x = imu_marker.scale.y = imu_marker.scale.z = 0.75;

            traj_marker.points.resize(keyframes.size());
            traj_marker.colors.resize(keyframes.size());
            for(int i=0; i<keyframes.size(); i++) {
                Eigen::Vector3d pos = keyframes[i]->graphNode->estimate().translation();
                traj_marker.points[i].x = pos.x();
                traj_marker.points[i].y = pos.y();
                traj_marker.points[i].z = pos.z();

                double p = static_cast<double>(i) / keyframes.size();
                traj_marker.colors[i].r = 1.0 - p;
                traj_marker.colors[i].g = p;
                traj_marker.colors[i].b = 0.0;
                traj_marker.colors[i].a = 1.0;

                if(keyframes[i]->acceleration) {
                    Eigen::Vector3d pos = keyframes[i]->graphNode->estimate().translation();
                    geometry_msgs::Point point;
                    point.x = pos.x();
                    point.y = pos.y();
                    point.z = pos.z();

                    std_msgs::ColorRGBA color;
                    color.r = 0.0;
                    color.g = 0.0;
                    color.b = 1.0;
                    color.a = 0.1;

                    imu_marker.points.push_back(point);
                    imu_marker.colors.push_back(color);
                }   
            }
            return markers;
        }

        ros::NodeHandle m_nodeHandle;
        ros::NodeHandle m_mtNodeHandle;
        ros::NodeHandle m_privNodeHandle;

        double m_dOptimizationInterval, m_dMapPublishInterval;
        ros::WallTimer m_timerOptimization;
        ros::WallTimer m_timerMapPublish;
        ros::WallTimer m_timerOdom2Map;

        std::unique_ptr<message_filters::Subscriber<nav_msgs::Odometry>> m_odomSubscriber;
        std::unique_ptr<message_filters::Subscriber<sensor_msgs::PointCloud2>> m_cloudSubscriber;
        std::unique_ptr<message_filters::TimeSynchronizer<nav_msgs::Odometry, sensor_msgs::PointCloud2>> m_odomCloudSync;

        std::mutex m_mutexMain;

        // GPS
        std::string m_sGPSTopic;
        std::string m_sNMEATopic;
        std::string m_sNavSatTopic;
        ros::Subscriber m_gpsSubscriber;
        ros::Subscriber m_nmeaSubscriber;
        ros::Subscriber m_navsatSubscriber;
        std::mutex m_mutexGPS;
        boost::shared_ptr<GpsHandler> m_gpsHandler; // needs to be a boost::shared_ptr in order to pass it for subscriber cbs

        // IMU
        std::string m_sImuTopic;
        ros::Subscriber m_imuSubscriber;
        std::mutex m_mutexImu;
        boost::shared_ptr<ImuHandler> m_imuHandler; // needs to be a boost::shared_ptr in order to pass it for subscriber cbs

        // Transformation frames
        std::string m_sBaseFrame;
        std::string m_sMapFrame;
        std::string m_sOdomFrame;

        Eigen::Matrix4f m_transOdom2Map;
        tf::TransformBroadcaster m_transOdom2MapBroadcaster;
        ros::Publisher m_odom2mapPublisher;

        // Loop closure
        std::unique_ptr<LoopHandler> m_loopHandler;

        // Map generation
        double m_dMapCloudResolution;
        ros::Publisher m_mapCloudPublisher;
        std::mutex m_mutexSnapshots;
        std::vector<KeyframeSnapshot::Ptr> m_vKeyframeSnapshots;
        std::unique_ptr<MapBuilder> m_mapBuilder;

        // Keyframes
        std::shared_ptr<KeyframeHandler> m_keyframeHandler;
        std::mutex m_mutexKeyframes;

        // GraphSLAM
        std::shared_ptr<GraphSLAM> m_graphSlam;
        std::string m_sG2OSolverType;

        //Markers
        ros::Publisher m_markersPublisher;
    };
}

PLUGINLIB_EXPORT_CLASS(graph_slam::GraphSlamNodelet, nodelet::Nodelet)
